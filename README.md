# E008 &ndash; Spike Detection by High-pass Filtering and Thresholding of Wideband Channels in V206's PLX files

[![](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lcg%2Fpasad%2Fnotebooks%2Fe008-spike-detection/master)
[![](https://img.shields.io/badge/Jira-VIZPIKE--12-blue.svg)](https://pedroasad.atlassian.net/browse/VIZPIKE-12).

## Introduction

### Goals

* Verify **if**
  * V206's spike channel data corresponds to a simple high-pass filtering, followed by thresholding, of corresponding wideband channels, **or**
  * If there seems to be some more processing applied by Plexon software.
* Estimate how fast can spike data be recomputed as filtering and thresholding options are changed?
  Can it be made interactive in a satisfactory way (from a user experience perspective)?

## Usage

### Requirements

* [Docker](https://docs.docker.com)
* [rclone](https://rclone.org/) (optional)

### Instructions

1. Download the necessary files from [LCG's Google Drive folders](https://drive.google.com/drive/u/1/folders/1UieuXEmeroAteDgu7VvN1oJW7mc5o2YB) into `data/`.
   If you have [rclone](https://rclone.org/) installed, you can:
   * Setup a remote (say `lcg-drive`) by running `rclone config`, choosing the __Google Drive__ remote type, and answering a few more questions (leaving the defaults in most should work), then
   * Run
     ```bash
     rclone copy lcg-drive:Neuroscience/Datasets/ProjectV2/data/V206/Dados/v206/${PLX_FILE} data/ -vv
     ```
1. Build and run the environment with

   ```bash
   docker build -t notebooks/e008 .
   docker run --rm \
          -d \
          -e CHOWN_HOME=yes \
          -e CHOWN_HOME_OPTS=-R \
          -e JUPYTER_TOKEN="" \
          -e NB_UID=$(id -u) \
          -e JUPYTER_THEME_OPTIONS="-cellw 90% --kernellogo --nbname --theme monokai --toolbar" \
          --name nbe008 \
          -p 8000:8888 \
          --user root \
          -v "$PWD":/home/jovyan/work \
          -v "$HOME/.ipython":/home/jovyan/.ipython \
          -v "$HOME/.jupyter":/home/jovyan/.jupyter \
          notebooks/e008
   docker exec nbe008 bash -c "cd work && dvc repro"  # This will pre-process data. Wait for it to finish before executing notebooks that depend on this data.
   ```
   then, visit http://localhost:8000 &mdash; it may take a few seconds to work, because of `chown -R`.

**Notes:**
* Some of the arguments to `docker run` shown above (like Jupyter theme options) are evidently optional.
* Execute the notebooks in order, as there is some intermediate data generated by them that is not included in a DVC stage.
* If you set up DVC's cache mode to symlinking to a different disk (*.e.g*, `dvc config --local cache.type symlink; dvc config --local cache.dir /data/e008.dvc-cache`, when `/data` is at a different disk than the repository), remember to also bind-mount this directory in the container (in this example, add `-v /data/e008.dvc-cache:/data/e008.dvc-cache`).

More information about the `jupyter/scipy-notebook` can be found [here](https://jupyter-docker-stacks.readthedocs.io/en/latest/).
For instructions on making sure the [Dockerfile](./Dockerfile) is compatible with [Binder](https://mybinder.org), see [this page](https://mybinder.readthedocs.io/en/latest/tutorials/dockerfile.html#preparing-your-dockerfile).
