{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# NB01 &ndash; Data preparation and notes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>Introduction</a></span></li><li><span><a href=\"#Pre-exporting-some-PLX-channel-data\" data-toc-modified-id=\"Pre-exporting-some-PLX-channel-data-2\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>Pre-exporting some PLX channel data</a></span></li><li><span><a href=\"#Conclusions\" data-toc-modified-id=\"Conclusions-3\"><span class=\"toc-item-num\">3&nbsp;&nbsp;</span>Conclusions</a></span></li><li><span><a href=\"#References\" data-toc-modified-id=\"References-4\"><span class=\"toc-item-num\">4&nbsp;&nbsp;</span>References</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "We need to:\n",
    "\n",
    "1. [X] Find out which parameters (high-pass filtering and automatic threshold) are applied.\n",
    "   According to the OmniPlex manual:\n",
    "   * 4-pole Bessel low/high-pass filtering (may also be Butterworth, or Elliptic) &mdash; check out [scipy's Bessel filter](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.bessel.html)\n",
    "   * Lowpass cutoff at 200 Hz, followed by downsampling to 1 KHz, to obtain the LFP\n",
    "   * Highpass cutoff at 300 Hz to obtain the spike continuous (SPKC) signal\n",
    "   * There are a variety of automatic spike threshold settings available &mdash; **understand these better**\n",
    "1. [X] Find a PLX file with matching continuously sampled (WB) and filtered spike (SPKC) channels.\n",
    "   The gratings file from [this report](https://lcg.gitlab.io/pasad/dsc/journal/f8d10c1c21/lfc-paper/corner-cases.run.html) is a decent candidate.\n",
    "1. [X] Export WB, LFP, SPKC, and SPK channels\n",
    "   * Using [lcg-neuro-plx](https://pypi.org/project/lcg-neuro-plx) in the next section.\n",
    "   * As an alternative, we could use HDF5 to enable:\n",
    "     * Compression\n",
    "       * HDF5 compression options (refer to [the manual](https://support.hdfgroup.org/HDF5/doc/UG/HDF5_Users_Guide.pdf) or to [h5py's documentation](http://docs.h5py.org/en/stable/))\n",
    "       * Use a proper signal compression technique, like Wavelet transform\n",
    "       * [Lossless Compression of Neural Signals with Predictor Schemes Achieving more than fivefold data Reduction of in-vitro Recorded Retinal Signals](https://www.frontiersin.org/10.3389%2Fconf.fncel.2018.38.00034/event_abstract)\n",
    "     * Avoid using [lcg-neuro-plx](https://pypi.org/project/lcg-neuro-plx) for things like reading\n",
    "     * Store additional data, like Fourier transforms\n",
    "1. [X] Inspect signals &ndash; done in [NB02](nb02-inspecting-signals.ipynb)\n",
    "1. [X] Apply filtering/thresholding on WB channel to get LFP/SPKC channels &ndash; done in [NB05](nb05-spike-detection.ipynb)\n",
    "1. [X] Compare output with original LFP/SPKC channels.\n",
    "   * Box-filtering (as suggested by Mario Fiorani) causes moderate deformation of spikes &mdash; since box diameters are inherently realted to a tradeoff between riding the LFP and preserving the spiking activity.\n",
    "   * Reproducing what the manual says gives interesting results, but not identical &mdash; waveforms are considerably different, but I would argue they are better.\n",
    "   * It seems that the spikes in PLX files were not aligned by the depolarization peak, and that doing so produces more consistent waveform statistics (which should improve sorting)\n",
    "\n",
    "**Next steps:**\n",
    "* Compute signal features as documented in the Omniplex manual &mdash; it does not simply apply PCA to the waveform samples, but rather to waveform features, *e.g.* the difference between peaks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pre-exporting some PLX channel data\n",
    "\n",
    "Follow the instructions in the [README](https://gitlab.com/lcg/pasad/notebooks/e008-spike-detection/-/blob/master/README.md)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions\n",
    "\n",
    "* Preprocessing the signals to obtain LFP and SPKC from WB, following the manual directions, is relatively straightforward (using `scipy.signal`'s [functions](https://docs.scipy.org/doc/scipy/reference/signal.html)).\n",
    "* Spike detection can be achieved by inspecting candidate timestamps (threshold-crossing points), but\n",
    "  * The right threshold is highly debatable, therefore we should do it for a number of thresholds (say $\\mu - n\\sigma$, for $4 \\leq \\sigma \\leq 7$, where $\\mu$ is SPKC's mean and $\\sigma$ its standard deviation), and\n",
    "  * This can take a while for lower thresholds, which would suggest parallelization, but it is relatively fast for $n \\geq 3$, as the number of threshold-crossing points drops rapidly with increasing $n$.\n",
    "* There exist many ways to filter a signal, including\n",
    "  * Filter types:\n",
    "    * [Bessel](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.bessel.html)\n",
    "    * [Butter](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.butter.html)\n",
    "    * [Elliptic](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.ellip.html)\n",
    "  * Filtering strategies\n",
    "    * Numerator/denominator (`output='ba'`) [forward filtering](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lfilter.html)\n",
    "    * Second order sections (`output='sos'`) [forward filtering](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.sosfilt.html)\n",
    "    * Numerator/denominator (`output='ba'`) [forward-backward filtering](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.filtfilt.html)\n",
    "    * Second order sections (`output='sos'`) [forward-backward filtering](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.sosfiltfilt.html)\n",
    "  Each of these filtering modes has its own pros and cons (*e.g.* waveform deformation, SPKC flattening, LFP/SPKC shifting).\n",
    "  4-pole Bessel forward-backward filtering produces no shifting, whereas its forward variant (the one used in [NB05](nb05-spike-detection.ipynb)) shifts the signal a bit, but produces better waveforms (with a consistent drop to signal baseline after repolarization).\n",
    "  * **Notes:**\n",
    "    * We can use a \"good-looking\" variant and unshift it by fitting the resulting LFP to the one obtained by forward-backward filtering.\n",
    "    * **I would dare say** that the WB shifting we see when plotting the signal is itself a result of forward-filtering the raw input signal, which I think the Omniplex system does at some point before recording in the PLX file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "* [OmniPlex Manual](https://plexon.com/wp-content/uploads/2020/01/OmniPlex-User-Guide.pdf)\n",
    "  * Section 2.7: Separating Wideband Signal into Field Potentials and Spikes\n",
    "  * Chapter 5: Spike Detection\n",
    "  * Chapter 6: Basic Spike Sorting\n",
    "  * Chapter 8: Additional Sorting Methods and Quality Metrics\n",
    "  * Section 12.5.2: Uses of the Live Filter Adjustment\n",
    "  * Appendix B: Separation of Spikes and Field Potentials Using Digital Filters\n",
    "  * Appendix G: Robust Statistics\n",
    "* [Offline Sorter](https://plexon.com/wp-content/uploads/2020/01/Offline-Sorter-v4-User-Guide.pdf)\n",
    "* [Denoising data with FFT [Python]](https://www.youtube.com/watch?v=s2K1JfNR7Sc)\n",
    "* [What is the Fourier Transform? A Visual Introduction](https://www.youtube.com/watch?v=spUNpyF58BY)\n",
    "* [Binary structure of PLX files](https://lcg.gitlab.io/neuro/python-plx/plx.html)\n",
    "* [Scipy docs](https://docs.scipy.org/doc/scipy/reference/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
