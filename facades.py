import attr
import numpy as np
import plx

from enum import Enum
from functools import cached_property, lru_cache


class SlowChannelType(Enum):
    WB = 'WB'
    LFP = 'FP'
    SPKC = 'SPKC'
    
    
@attr.s(frozen=True, hash=False)
class SlowChannelFacade:
    plx_file: plx.AbstractPlxFile = attr.ib()
    slow_channel: plx.SlowChannel = attr.ib()
    type: SlowChannelType = attr.ib()
        
    @cached_property
    def denorm_factor(self) -> float:
        recording_header = self.plx_file.header
        slow_header = self.slow_channel.header
        plx_version = recording_header['Version']
        
        if plx_version < 102:
            max_magnitude = 5000
            digital_levels = 2048
            preamp_gain = 1000
        elif plx_version < 103:
            max_magnitude = 5000
            digital_levels = 2048
            preamp_gain = slow_header['PreAmpGain']
        else:
            max_magnitude = recording_header['SlowMaxMagnitudeMV']
            digital_levels = 2**(recording_header['BitsPerSlowSample'] - 1)
            preamp_gain = slow_header['PreAmpGain']
            
        factor = max_magnitude / (digital_levels * preamp_gain * slow_header['Gain'])
        return factor
    
    @cached_property
    def max_amplitude(self) -> float:
        return np.max(np.abs(self.signal))
    
    @property
    def name(self) -> str:
        return self.slow_channel.header['Name']
        
    @property
    def sampling_rate(self) -> float:
        return self.slow_channel.header['ADFreq']
    
    @property
    @lru_cache
    def signal(self) -> np.ndarray:
        array = self.slow_channel.data.astype('f4')
        array *= self.denorm_factor
        return array
    
    @property
    def total_samples(self) -> int:
        return self.slow_channel.data.shape[0]
    
    @property
    def total_time(self) -> float:
        return self.total_samples / self.sampling_rate
    
    def __hash__(self):
        return hash((
            self.plx_file,
            self.slow_channel.header['Name'],
            type
        ))
    

@attr.s(frozen=True, hash=False)
class SpikeChannelFacade:
    plx_file: plx.AbstractPlxFile = attr.ib()
    spike_channel: plx.SpikeChannel = attr.ib()
        
    @property
    def channel(self) -> int:
        return self.spike_channel.header['Channel']
        
    @cached_property
    def denorm_factor(self) -> float:
        recording_header = self.plx_file.header
        spike_header = self.spike_channel.header
        plx_version = recording_header['Version']
        
        if plx_version < 103:
            max_magnitude = 3000
            digital_levels = 2048
            preamp_gain = 1000
        elif plx_version < 105:
            max_magnitude = recording_header['SpikeMaxMagnitudeMV']
            digital_levels = 2**(recording_header['BitsPerSpikeSample'] - 1)
            preamp_gain = 1000
        else:
            max_magnitude = recording_header['SpikeMaxMagnitudeMV']
            digital_levels = 2**(recording_header['BitsPerSpikeSample'] - 1)
            preamp_gain = recording_header['SpikePreAmpGain']
            
        factor = max_magnitude / (digital_levels * preamp_gain * spike_header['Gain'])
        return factor
    
    @cached_property
    def max_wf_amplitude(self) -> float:
        return np.max(np.abs(self.wfs))
    
    @property
    def name(self) -> str:
        return self.spike_channel.header['Name']
    
    @property
    def sampling_rate(self) -> float:
        return self.plx_file.header['ADFrequency']
    
    @property
    def spike_count(self) -> int:
        return len(self.spike_channel.data)
    
    @property
    @lru_cache
    def times(self) -> np.ndarray:
        array = self.timestamps.astype('f4')
        array /= self.sampling_rate
        return array
    
    @property
    @lru_cache
    def timestamps(self) -> np.ndarray:
        return self.spike_channel.data['timestamp']
    
    @property
    def wf_threshold(self):
        return self.denorm_factor * self.spike_channel.header['Threshold']

    @property
    @lru_cache
    def wfs(self) -> np.ndarray:
        array = self.spike_channel.data['waveform'].astype('f4')
        array *= self.denorm_factor
        return array
    
    def __hash__(self):
        return hash((
            self.plx_file,
            self.spike_channel.header['Name'],
            type
        ))
        

@attr.s(frozen=True)
class RecordingFacade:
    name: str = attr.ib()
    plx_file: plx.ExportedPlxFile = attr.ib()
    
    @classmethod
    @lru_cache
    def load(cls, name: str) -> 'Recording':
        plx_file = plx.ExportedPlxFile(f"data/{name}")
        return cls(name, plx_file)
    
    @lru_cache
    def slow_channel(self, channel: int, type: SlowChannelType) -> SlowChannelFacade:
        channel_name = f"{type.value}{channel:02d}"
        for header in self.plx_file.slow_headers:
            if header['Name'] == channel_name:
                facade = SlowChannelFacade(
                    self.plx_file,
                    self.plx_file.slow_channel(header['Channel']+1),
                    type,
                )
                return facade
        raise ValueError(f"No such channel: {channel_name}")
    
    @lru_cache
    def spike_channel(self, channel: int) -> SpikeChannelFacade:
        print(f'Getting spike channel {channel}')
        facade = SpikeChannelFacade(
            self.plx_file,
            self.plx_file.spike_channel(channel),
        )
        return facade
