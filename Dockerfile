FROM jupyter/scipy-notebook:6d42503c684f

USER root
RUN apt-get update && apt-get install -y fonts-firacode
USER jovyan

RUN pip install \
    autopep8 \	
    jupyter_contrib_nbextensions \
    jupyterthemes

RUN jupyter contrib nbextension install --user
ARG ENABLE_NBEXTENSIONS="code_prettify/autopep8 codefolding/main toc2/main"
RUN for EXT in ${ENABLE_NBEXTENSIONS}; do \
    echo "Enabling nbextension: ${EXT}"; \
    jupyter nbextension enable "${EXT}"; \
done

WORKDIR $HOME

COPY requirements.txt .
RUN pip install -r requirements.txt
	
COPY *.ipynb work/

ENV JUPYTER_THEME_OPTIONS=""

CMD ["bash", "-c", "jt ${JUPYTER_THEME_OPTIONS}; start-notebook.sh --NotebookApp.token='' --NotebookApp.custom_display_url='http://localhost:8000'"]

